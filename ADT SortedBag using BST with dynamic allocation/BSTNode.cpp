#include "BSTNode.h"

BSTNode::BSTNode()
{
    this->value = NULL_TCOMP;
    this->leftSon = -1;
    this->rightSon = -1;
}

BSTNode::BSTNode(TComp value, int leftSon, int rightSon) {
    this->value = value;
    this->leftSon = leftSon;
    this->rightSon = rightSon;
}

TComp BSTNode::getValue() const {
    return value;
}

void BSTNode::setValue(TComp value) {
    this->value = value;
}

int BSTNode::getLeftSon() const {
    return leftSon;
}

void BSTNode::setLeftSon(int leftSon) {
    this->leftSon = leftSon;
}

int BSTNode::getRightSon() const {
    return rightSon;
}

void BSTNode::setRightSon(int rightSon) {
    this->rightSon = rightSon;
}

bool BSTNode::isNull() {
    return value == -11111;
}

bool BSTNode::isLeaf() {
    return leftSon == -1 && rightSon == -1;
}
