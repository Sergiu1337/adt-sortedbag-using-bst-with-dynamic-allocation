#pragma once
#include "SortedBag.h"
#include <stack>
using namespace std;

class SortedBag;
typedef int TComp;

class SortedBagIterator {
    friend class SortedBag;

private:
    const SortedBag& sb;
    int currentNode;
    stack<int> stack;
    SortedBagIterator(const SortedBag& sb);

public:
    TComp getCurrent();
    bool valid();
    void next();
    void first();
};