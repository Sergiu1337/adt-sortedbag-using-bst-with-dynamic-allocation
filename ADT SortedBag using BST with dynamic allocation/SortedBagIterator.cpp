#include "SortedBagIterator.h"
#include "SortedBag.h"
#include <exception>

SortedBagIterator::SortedBagIterator(const SortedBag& sb) : sb{ sb }, stack{ std::stack<int>{} } {
    first();
}

void SortedBagIterator::first() {
    currentNode = sb.root;
    if(!this->stack.empty()){
        while(!this->stack.empty()){
            this->stack.pop();
        }
    }
    while (currentNode != -1) {
        stack.push(currentNode);
        currentNode = sb.tree[currentNode].getLeftSon();
    }

    if (stack.empty() == false) {
        currentNode = stack.top();
    }
    else {
        currentNode = -1;
    }
}

void SortedBagIterator::next() {
    if (!valid()) {
        throw exception();
    }

    int node = stack.top(); // start from the top of the stack
    stack.pop();

    // if there is a right son, we go there and start going left until empty and add them to the stack
    if (sb.tree[node].getRightSon() != -1) {
        node = sb.tree[node].getRightSon();
        while (node != -1) {
            stack.push(node);
            node = sb.tree[node].getLeftSon();
        }
    }

    if (stack.empty() == false) {
        // if stack != empty then position = top
        currentNode = stack.top();
    }
    else {
        // otherwise position = -1 = null
        currentNode = -1;
    }
}

bool SortedBagIterator::valid() {
    return currentNode != -1;
}

TComp SortedBagIterator::getCurrent() {
    if (!valid()) {
        throw exception();
    }
    return sb.tree[currentNode].getValue();
}
