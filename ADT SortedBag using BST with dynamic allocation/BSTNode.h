#pragma once

typedef int TComp;
typedef TComp TElem;
typedef bool(*Relation)(TComp, TComp);
#define NULL_TCOMP -11111;

// Class that represents a BST's Node
class BSTNode {
private:
    TComp value;
    int leftSon;
    int rightSon;

public:
    BSTNode();
    BSTNode(TComp value, int leftSon, int rightSon);

    TComp getValue() const;
    void setValue(TComp value);

    int getLeftSon() const;
    void setLeftSon(int leftSon);

    int getRightSon() const;
    void setRightSon(int rightSon);

    bool isNull();

    bool isLeaf();
};
