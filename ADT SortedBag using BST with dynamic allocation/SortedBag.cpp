#include "SortedBag.h"
#include "SortedBagIterator.h"

SortedBag::SortedBag(Relation r) {
    this->r = r;
    this->root = -1;
    this->numberOfElements = 0;
    this->capacity = 10;
    this->firstFree = 0;
    this->tree = new BSTNode[this->capacity];
}

void SortedBag::add(TComp e) {
    if (this->firstFree >= this->capacity) {
        resize();
    }

    this->tree[firstFree].setValue(e);
    this->tree[firstFree].setLeftSon(-1);
    this->tree[firstFree].setRightSon(-1);

    int current = this->root, parent = -1;
    while (current != -1) {
        parent = current;
        if (this->r(e, this->tree[current].getValue()))
            current = this->tree[current].getLeftSon();
        else
            current = this->tree[current].getRightSon();
    }

    if (this->root == -1)
        this->root = this->firstFree;
    else if (this->r(e, tree[parent].getValue()))
        this->tree[parent].setLeftSon(this->firstFree);
    else
        this->tree[parent].setRightSon(this->firstFree);
    this->changeFirstFree();
    ++this->numberOfElements;
}

void SortedBag::changeFirstFree() {
    ++this->firstFree;
    while (this->firstFree < this->capacity && !this->tree[this->firstFree].isNull())
        ++this->firstFree;
}

bool SortedBag::remove(TComp e) {
    bool removed = false;
    this->root = removeRecursively(this->root, e, removed);
    if (removed)
        --this->numberOfElements;
    return removed;
}

bool SortedBag::search(TComp e) const {
    int currentNode = this->root;

    // ma duc pe fiul stang sau drept in functie de cum se stabileste relatia
    while (currentNode != -1) {
        if (tree[currentNode].getValue() == e)
            return true;
        if (this->r(e, this->tree[currentNode].getValue())) {
            currentNode = this->tree[currentNode].getLeftSon();
        }
        else {
            currentNode = this->tree[currentNode].getRightSon();
        }
    }
    return false;
}

int SortedBag::nrOccurrences(TComp e) const {
    int currentNode = this->root;
    int counter = 0;

    // la fel ca la search, nu e foarte greu de urmarit
    while (currentNode != -1) {
        if (this->tree[currentNode].getValue() == e)
            ++counter;
        if (this->r(e, this->tree[currentNode].getValue())) {
            currentNode = this->tree[currentNode].getLeftSon();
        }
        else {
            currentNode = this->tree[currentNode].getRightSon();
        }
    }
    return counter;
}

int SortedBag::size() const {
    return numberOfElements;
}

SortedBagIterator SortedBag::iterator() const {
    return SortedBagIterator(*this);
}

bool SortedBag::isEmpty() const {
    return numberOfElements == 0;
}

SortedBag::~SortedBag() {
    delete[] this->tree;
}

void SortedBag::resize() {
    BSTNode* newTree = new BSTNode[capacity * 2];
    for (int i = 0; i < capacity; ++i) {
        newTree[i].setValue(this->tree[i].getValue());
        newTree[i].setLeftSon(this->tree[i].getLeftSon());
        newTree[i].setRightSon(this->tree[i].getRightSon());
    }
    delete[] this->tree;
    this->tree = newTree;
    this->firstFree = this->capacity;
    this->capacity *= 2;
}

TComp SortedBag::getMaximum(int startingRoot) {
    int currentNode = startingRoot, maxNode = startingRoot;
    TComp maximum;

    while (currentNode != -1) {
        maximum = this->tree[currentNode].getValue();
        maxNode = currentNode;
        currentNode = this->tree[currentNode].getRightSon();
    }
    return maxNode;
}

int SortedBag::removeRecursively(int node, TComp e, bool& removed) {
    if (node == -1)
        return node;

    if (e == this->tree[node].getValue()) {
        // am dat de elementul pe care-l caut, tre sa-l sterg
        removed = true;
        if (this->tree[node].getLeftSon() == -1) { // daca nu exista un fiu stang, inseamna ca mutam subarborele care incepe
                                             // in fiul drept peste subarborele determinat de "node"
            int rightSon = this->tree[node].getRightSon();
            this->tree[node] = BSTNode();
            return rightSon;
        }
        else if (this->tree[node].getRightSon() == -1) { // analog ca mai sus, doar ca pt fiul drept, mutam subarborele
                                                   // care incepe in fiul stang
            int leftSon = tree[node].getLeftSon();
            this->tree[node] = BSTNode();
            return leftSon;
        }

        // altfel daca are ambii fii, punem valoarea celui mai mare element din subarborele stang
        // cu valoarea din nodul si stergem o aparitie a celui mai mare element (ca sa nu apara de 2 ori, ca e ca si
        // cum l-am muta pe ala in nodul curent
        int maxNode = getMaximum(tree[node].getLeftSon());
        tree[node].setValue(tree[maxNode].getValue());
        tree[node].setLeftSon(removeRecursively(tree[node].getLeftSon(), tree[maxNode].getValue(), removed));
    }
    else if (r(e, tree[node].getValue())) {
        // daca nodul se afla in subarborele determinat de fiul stang, atunci
        // fiul stang va fi radacina subarborelui determinat de initialul fiu stang dupa ce elementul cautat
        // a fost sters
        tree[node].setLeftSon(removeRecursively(tree[node].getLeftSon(), e, removed));
    }
    else {
        // la fel, doar ca pt fiul drept
        tree[node].setRightSon(removeRecursively(tree[node].getRightSon(), e, removed));
    }
    return node;
}
